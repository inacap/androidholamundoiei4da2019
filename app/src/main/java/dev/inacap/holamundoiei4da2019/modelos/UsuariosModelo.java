package dev.inacap.holamundoiei4da2019.modelos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class UsuariosModelo {
    private MainDBHelper dbHelper;

    public UsuariosModelo(Context contexto){
        this.dbHelper = new MainDBHelper(contexto);
    }

    public void crearUsuario(ContentValues usuario){
        SQLiteDatabase db = this.dbHelper.getWritableDatabase();
        db.insert(MainDBContract.MainDBUsuarios.NOMBRE_TABLA, null, usuario);
    }

    public ContentValues obtenerUsuario(String username){
        // La consulta basica
        String consulta = String.format("SELECT * FROM usuarios WHERE %s = ?", MainDBContract.MainDBUsuarios.COLUMNA_USERNAME);

        // El nombre de la columna, y el valor que queremos buscar
        String[] parametros = new String[]{
                username
        };

        // Abrimos la base de datos en modo lectura
        SQLiteDatabase db = this.dbHelper.getReadableDatabase();

        // Pedimos los datos
        Cursor cursor = db.rawQuery(consulta, parametros);

        // Nos movemos a la primera fila de los datos
        if(cursor.moveToFirst()){
            // Si entramos es por que hay datos

            // Sacar los datos del cursor
            String cursor_username = cursor.getString(cursor.getColumnIndex(MainDBContract.MainDBUsuarios.COLUMNA_USERNAME));
            String cursor_password = cursor.getString(cursor.getColumnIndex(MainDBContract.MainDBUsuarios.COLUMNA_PASSWORD));

            ContentValues usuario = new ContentValues();
            usuario.put(MainDBContract.MainDBUsuarios.COLUMNA_USERNAME, cursor_username);
            usuario.put(MainDBContract.MainDBUsuarios.COLUMNA_PASSWORD, cursor_password);

            return usuario;
        }


        return null;
    }
}
